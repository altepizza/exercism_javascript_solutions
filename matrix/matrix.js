//
// This is only a SKELETON file for the 'Matrix' exercise. It's been provided as a
// convenience to get you started writing code faster.
//

export class Matrix {

  constructor(matrixString) {
    this.aMatrix = matrixString.split('\n').map(x=>x.split(' '));
  }

  get rows() {
    if (Array.isArray(this.aMatrix)) {
      return this.aMatrix.map(x=>x.map(xx=>+xx));
    }
    return [this.aMatrix]
  }

  get columns() {
    var i;
    var someColumns = [];
    var noOfColumns = this.aMatrix[0].length;
    if (Array.isArray(this.aMatrix)) {
      for (i = 0; i < noOfColumns; i++) {
        someColumns.push(this.aMatrix.map(x=>x[i]));
      }
      return someColumns.map(x=>x.map(xx=>+xx));
    }
    return [this.aMatrix]
  }
}
