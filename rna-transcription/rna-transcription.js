const NUCLEOTIDE_REGEX = /[G, C, T, A]/g;

const TRANSLATION = {
  "G": "C",
  "C": "G",
  "T": "A",
  "A": "U",
};

function dnaToRNA(nucleotid) {
  return TRANSLATION[nucleotid];
};

export const toRna = (dna) => {
  return dna.replace(NUCLEOTIDE_REGEX, dnaToRNA);
};
