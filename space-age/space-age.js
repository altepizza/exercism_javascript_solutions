const EARTH_YEAR_IN_SEC = 31557600

const PLANET_TO_EARTH_FACTOR = {
  'earth': 1.0,
  'jupiter': 11.862615,
  'mars': 1.8808158,
  'mercury': 0.2408467,
  'neptune': 164.79132,
  'saturn': 29.447498,
  'uranus': 84.016846,
  'venus': 0.61519726,
};

function round_2_decimals(number) {
  return Math.round(number * 100) / 100;
}

export const age = (target_planet, seconds) => {
  let factor = PLANET_TO_EARTH_FACTOR[target_planet];
  let target_planet_years = seconds / EARTH_YEAR_IN_SEC / factor;
  return round_2_decimals(target_planet_years);
};
