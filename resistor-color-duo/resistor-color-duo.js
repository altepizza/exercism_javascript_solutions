export const COLORS = ['black', 'brown', 'red', 'orange', 'yellow', 'green', 'blue', 'violet', 'grey', 'white']

export const decodedValue = ([firstColor, secondColor]) => {
  return Number(`${resistanceValue(firstColor)}${resistanceValue(secondColor)}`)
};

const resistanceValue = color => COLORS.indexOf(color);
