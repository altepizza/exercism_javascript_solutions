//
// This is only a SKELETON file for the 'Gigasecond' exercise. It's been provided as a
// convenience to get you started writing code faster.
//
const _gigaSecond = 1000000000;

export const gigasecond = (oldDate) => {
  var futureDate = new Date(oldDate.getTime());
  futureDate.setUTCSeconds(oldDate.getUTCSeconds() + _gigaSecond);
  return futureDate;
};
