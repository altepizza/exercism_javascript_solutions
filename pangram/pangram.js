const ALPHABET_REGEX = /[a-z]/g
const ALPHABET_SET = new Set('abcdefghijklmnopqrstuvwxyz');

export const isPangram = (sentence) => {
  let uniqueChars = new Set(sentence.toLowerCase().match(ALPHABET_REGEX));
  return uniqueChars.size === ALPHABET_SET.size;
};
